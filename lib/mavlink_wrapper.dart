// ignore_for_file: non_constant_identifier_names

import 'dart:ffi';
import 'dart:io';

import 'mavlink_wrapper_type.dart';

final DynamicLibrary nativeAddLib = (Platform.isAndroid || Platform.isLinux)
  ? DynamicLibrary.open('build/linux/x64/debug/plugins/mavlink_plugin/libmavlink_wrapper.so') // ?
  : DynamicLibrary.process();

class MavlinkWrapper {
  //Các định nghĩa tiêu chuẩn 
  final int Function() getSizeMavlinkMessage = nativeAddLib // tra ve kich thuoc
  .lookup<NativeFunction<Uint16 Function()>>('getSizeMavlinkMessage')//NativeFunction bieu dien 1 kieu ham trong C trong C???
      .asFunction(); //Chuyển đổi thành hàm Dart, tự động sắp xếp các đối số và trả về giá trị

  final int Function() getSizeMavlinkStatus = nativeAddLib
      .lookup<NativeFunction<Uint16 Function()>>('getSizeMavlinkStatus')
      .asFunction();

  final int Function() getSizeHeartBeat = nativeAddLib
      .lookup<NativeFunction<Uint16 Function()>>('getSizeHeartBeat')
      .asFunction(); // tra ve kich thuoc cua kieu du lieu

  final int Function(
      int chan,
      int c,
      Pointer<MavlinkMessage> r_message,
      Pointer<MavlinkStatus> r_mavlink_status)
      mavlinkParseChar = nativeAddLib
      .lookup<NativeFunction<Uint16 Function(
          Uint8,
          Uint8,
          Pointer<MavlinkMessage>,
          Pointer<MavlinkStatus>)>>('mavlinkParseChar') //mavlink_helpers.h
      .asFunction();

  final int Function(
      Pointer<Uint8> buf,
      Pointer<MavlinkMessage> msg)
      mavlinkMsgToSendBuffer = nativeAddLib
      .lookup<NativeFunction<Uint16 Function(
          Pointer<Uint8>,
          Pointer<MavlinkMessage>)>>('mavlinkMsgToSendBuffer')// tra ve kich thuoc cua protocal da lam
      .asFunction();
/////////////////////////////////////////////////////////////////////////////////////////////
  final int Function(
      int system_id,
      int component_id,
      int chan,
      Pointer<MavlinkMessage> msg,
      Pointer<MavlinkHeartbeat> heartbeat)
      mavlinkMsgHeartbeatEncodeChan = nativeAddLib
      .lookup<NativeFunction<Uint16 Function(
          Uint8,
          Uint8,
          Uint8,
          Pointer<MavlinkMessage>,
          Pointer<MavlinkHeartbeat> heartbeat)>>('mavlinkMsgHeartbeatEncodeChan')
      .asFunction();

  final int Function(
      Pointer<MavlinkMessage> msg,
      Pointer<MavlinkHeartbeat> heartbeat)
      mavlinkMsgHeartbeatDecode = nativeAddLib
      .lookup<NativeFunction<Uint16 Function(
          Pointer<MavlinkMessage>,
          Pointer<MavlinkHeartbeat> heartbeat)>>('mavlinkMsgHeartbeatDecode') ////Giai ma cau truc msg thanh cau truc hearbeat da lam
      .asFunction();
}
