#pragma once

#include <stdint.h>

#include "mavlink/common/mavlink.h"

extern "C" __attribute__((visibility("default"))) __attribute__((used)) //Thuộc tính biến này thông báo cho trình biên dịch rằng một biến tĩnh sẽ được giữ lại trong tệp đối tượng, ngay cả khi nó không được tham chiếu.
inline uint16_t getSizeMavlinkMessage(void) {
  return sizeof(mavlink_message_t);//co nghia ham nay se tra về  so byte
}

extern "C" __attribute__((visibility("default"))) __attribute__((used))
inline uint16_t getSizeMavlinkStatus(void) {
  return sizeof(mavlink_status_t); //sizeof nhận một tham số là bất kỳ kiểu dữ liệu nào và trả về kích cỡ của kiểu dữ liệu đó. 
}                                 //co nghia ham nay se tra về  so byte

extern "C" __attribute__((visibility("default"))) __attribute__((used))
inline uint16_t getSizeHeartBeat(void) {
  return sizeof(mavlink_heartbeat_t); //Nhan 1 thong so bat ky kieu du lieu  nào, Tra ve kich co cua kieu du liệu đó
}

// Đây là một hàm tiện lợi xử lý phân tích cú pháp MAVLink hoàn chỉnh.
// Hàm sẽ phân tích cú pháp từng byte một và trả về gói tin hoàn chỉnh một lần,
// nó có thể được giải mã thành công. Hàm này sẽ trả về 0 hoặc 1

extern "C" __attribute__((visibility("default"))) __attribute__((used))
inline uint16_t mavlinkParseChar(
    uint8_t chan, //ID của kênh được phân tích cú pháp
    uint8_t c, //The char to parse
    mavlink_message_t *r_message,//Khoi tao gia tri con tro nhung return ve dia chi ma con tro dang tro den
    mavlink_status_t *r_mavlink_status) {
  return mavlink_parse_char(chan, c, r_message, r_mavlink_status); //Hàm này sẽ trả về 0 hoặc 1
}

extern "C" __attribute__((visibility("default"))) __attribute__((used))
inline uint16_t mavlinkMsgToSendBuffer(
    uint8_t *buf,
    const mavlink_message_t *msg) {
  return mavlink_msg_to_send_buffer(buf, msg);// tra ve kich thuoc cua protocal
}

extern "C" __attribute__((visibility("default"))) __attribute__((used))
inline uint16_t mavlinkMsgHeartbeatEncodeChan(
    uint8_t system_id,
    uint8_t component_id,
    uint8_t chan,
    mavlink_message_t* msg,
    const mavlink_heartbeat_t* heartbeat) {
  return mavlink_msg_heartbeat_encode_chan(
      system_id,
      component_id,
      chan,
      msg,
      heartbeat);
}

extern "C" __attribute__((visibility("default"))) __attribute__((used))
inline void mavlinkMsgHeartbeatDecode(
    const mavlink_message_t* msg,
    mavlink_heartbeat_t* heartbeat) {
  return mavlink_msg_heartbeat_decode(msg, heartbeat); //Giai ma cau truc msg thanh cau truc hearbeat
}
