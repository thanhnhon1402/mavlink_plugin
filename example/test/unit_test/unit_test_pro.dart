// ignore_for_file: avoid_print, non_constant_identifier_names, constant_identifier_names
import 'dart:ffi'; 
import 'package:ffi/ffi.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mavlink_plugin/mavlink_wrapper.dart';
import 'package:mavlink_plugin/mavlink_wrapper_type.dart';

const heartBeat_result = [0, 2, 3, 81, 3, 3];

const event = [
  [253, 9, 0, 0, 46, 1, 1, 0, 0, 0, 0, 0, 0, 0, 2, 3, 81, 3, 3, 116, 181],
  [253, 9, 0, 0, 47, 1, 1, 0, 0, 0, 0, 0, 0, 0, 2, 3, 81, 3, 3, 100, 59],
  [253, 9, 0, 0, 150, 1, 1, 0, 0, 0, 0, 0, 0, 0, 2, 3, 81, 3, 3, 44, 43],
  [253, 9, 0, 0, 149, 1, 1, 0, 0, 0, 0, 0, 0, 0, 2, 3, 81, 3, 3, 13, 177],
  [253, 14, 0, 0, 186, 1, 1, 111, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 84, 96, 100, 240, 3, 169, 31],
];

//The return SizeMess value = 291, value expect 250-->320;
const SizeMessMin = 250, SizeMessMax = 320;

//The return SizeMess value = 45, value expect 30-->45;
const SizeStatusMin = 30, SizeStatusMax = 45;

//The return SizeMess value = 9, value expect 8-->15;
const SizeHearBeatMin =8, SizeHearBeatMax = 15;

bool compare_list(var result, var heartBeat) {
  if (result[0] == heartBeat.custom_mode &&
      result[1] == heartBeat.type &&
      result[2] == heartBeat.autopilot &&
      result[3] == heartBeat.base_mode &&
      result[4] == heartBeat.system_status &&
      result[5] == heartBeat.mavlink_version) {
    return true;
  } else {
    return false;
  }
}

compare_list_pro(var list1, var list2, int begin, int len){
  int k = 0;
  for(int i = begin; i <len; i++){
    if(list1[i] != list2[i]){
      k=1;
      break;
    }
  }
  if( k==1) return false;
  if (k== 0) return true;
}

void test_value_const(){
  group('The function returns a constant value:',(){

    test('getSizeMavlinkMessage error', () {
      var SizeMess = MavlinkWrapper().getSizeMavlinkMessage();

      expect(SizeMess > SizeMessMin && SizeMess < SizeMessMax, true);
    });

    test('getSizeMavlinkStatus error', () {
      var SizeStatus = MavlinkWrapper().getSizeMavlinkStatus();
      
      expect(SizeStatus > SizeStatusMin && SizeStatus < SizeStatusMax, true); 
    });

    test('getSizeHeartBeat error', () {
      var SizeHearBeat = MavlinkWrapper().getSizeHeartBeat();

      expect(SizeHearBeat > SizeHearBeatMin && SizeHearBeat < SizeHearBeatMax, true);
    });
});
}

void variable_value_function(){
  group('The function returns variable value', (){
    test('mavlinkMsgToSendBuffer', () {

      var receiveMavlinkStatus = calloc<MavlinkStatus>(MavlinkWrapper().getSizeMavlinkStatus());
      var msg = calloc<MavlinkMessage>(MavlinkWrapper().getSizeMavlinkMessage());

      for (final parse in event) {
        for (final c in parse) {
          var parseStatus = MavlinkWrapper().mavlinkParseChar(
              0,
              c, 
              msg,
              receiveMavlinkStatus);
          if (parseStatus == 1) {

            expect(c, parse[parse.length - 1]);
            var sendBuffer = calloc<Uint8>(100);
            var sendBufferLen = MavlinkWrapper().mavlinkMsgToSendBuffer(sendBuffer, msg);
            var sendBufferList = sendBuffer.asTypedList(sendBufferLen);

            expect(compare_list_pro(sendBufferList, parse, 0, sendBufferList.length), true);
            expect(sendBufferLen == parse.length,true); 
          }
        }
      }
    });

  test('mavlinkMsgToSendBuffer', () {

      var receiveMavlinkStatus = calloc<MavlinkStatus>(MavlinkWrapper().getSizeMavlinkStatus());
      var msg = calloc<MavlinkMessage>(MavlinkWrapper().getSizeMavlinkMessage());

      for (final parse in event) {
        // print(parse);
        for (final c in parse) {
          var parseStatus = MavlinkWrapper().mavlinkParseChar(
              0,
              c, 
              msg,
              receiveMavlinkStatus);
          if (parseStatus == 1) {

            expect(c, parse[parse.length - 1]);
            var sendBuffer = calloc<Uint8>(100);
            var sendBufferLen = MavlinkWrapper().mavlinkMsgToSendBuffer(sendBuffer, msg);
            var sendBufferList = sendBuffer.asTypedList(sendBufferLen);

            expect(compare_list_pro(sendBufferList, parse, 0, sendBufferList.length), true);
            expect(sendBufferLen == parse.length,true); 
          }
        }
      }
    });

   test('mavlinkMsgHeartbeatEncodeChan', () {

      var receiveMavlinkStatus = calloc<MavlinkStatus>(MavlinkWrapper().getSizeMavlinkStatus());
      var receiveMavlinkMessage = calloc<MavlinkMessage>(MavlinkWrapper().getSizeMavlinkMessage());

      for (final parse in event) {
        for (final c in parse) {
          var parseStatus = MavlinkWrapper().mavlinkParseChar(
              0, 
              c, 
              receiveMavlinkMessage,
              receiveMavlinkStatus);
              
          var receiveMavlinkMessageRef = receiveMavlinkMessage.ref;

          if (parseStatus == 1) {

            expect(c, parse[parse.length - 1]);
            if (receiveMavlinkMessageRef.msgid == 0) {

              var heartbeat = calloc<MavlinkHeartbeat>(MavlinkWrapper().getSizeHeartBeat());

              MavlinkWrapper().mavlinkMsgHeartbeatDecode(receiveMavlinkMessage, heartbeat);
              var EncodeChan = MavlinkWrapper().mavlinkMsgHeartbeatEncodeChan(0 ,0, 0,receiveMavlinkMessage,heartbeat);
              var sendBuffer = calloc<Uint8>(100);
              var sendBufferLen = MavlinkWrapper().mavlinkMsgToSendBuffer(sendBuffer, receiveMavlinkMessage);
              var sendBufferList = sendBuffer.asTypedList(sendBufferLen);

              expect(compare_list_pro(sendBufferList, parse, 9, sendBufferList.length-2), true);
              expect(EncodeChan == parse.length, true); 
            }
          }
        }
      }
    });
  });

}
void main() {

  test_value_const();

  variable_value_function();
}


//flutter test test/unit_test/unit_test_pro.dart